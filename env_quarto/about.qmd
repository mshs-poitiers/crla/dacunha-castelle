---
title: "A propos de ce site"


---
![](./imgs/DDC.jpg)

## Qu'est ce que le projet Fonds d'archives Didier Dacunha-Castelle ? 


Ce site présente les données du Fonds d'archives Didier Dacunha-Castelle, déposées dans l'entrepôt Nakala.
 
Ce fonds d'archives émane du projet CUBANEXUS - Les médiateurs culturels et scientifiques entre Cuba et l'Europe francophone (1952-1971).

## Le projet CUBANEXUS (Projet-ANR-21-CE27-0003)

Ce projet s’efforce d’étudier le rôle des médiateurs culturels et scientifiques entre Cuba et la francophonie européenne (France, Belgique, Suisse) de 1952 à 1971.
Enjeux et objectif :

Quatre objectifs de recherche ont été définis : 
1) Identifier les principaux médiateurs scientifiques et culturels entre les deux régions ; 
2) Évaluer leur influence dans le façonnement des interactions culturelles et scientifiques, ainsi que dans les représentations sociales de chaque communauté ; 
3) Ériger un réseau de chercheurs cubains (Université de La Havane, Casa de las Américas, Institut d’histoire de Cuba) et européens (CRLA-Archivos, STSLab) ; 
4) Produire un certain nombre de « livrables » (« fonds Cuba », inventaire des médiateurs, livre traduit en espagnol, deux articles, conférence, exposition de photos).


## Partenaires scientifiques

:::  {layout-ncol=3}

[![](./imgs/logo_item.png)](http://www.item.ens.fr/)

[![IHC Instituto de Historia / Grupo de Estudios Neo-coloniales](./imgs/ihc-logo.jpg)](https://www.facebook.com/people/Instituto-de-Historia-de-Cuba/100066682455236/)

[![STS Lab Université de Lausanne / Etudes sociales des sciences (STS Lab)](./imgs/unil-logo.png)](https://www.unil.ch/stslab/home.html)

[![CASA Casa de las Américas / Centro de Investigaciones Literarias](./imgs/casa-de-las-americas.jpg){width=200}](https://fr.wikipedia.org/wiki/Casa_de_las_Am%C3%A9ricas)

[![UH Universidad de La Habana / Departamento de Historia](./imgs/uh-logo.png)](https://www.uh.cu/carrera/historia/)

:::

## Partenaires financiers

:::  {layout-ncol=3}

[![Projet-ANR-21-CE27-0003](./imgs/ANR-logo-2021-sigle.jpg){width=300}](https://anr.fr/Projet-ANR-21-CE27-0003)

:::

## Partenaires techniques

::: {layout-ncol=3}

[![](https://app.francoralite.net/static/francoralite_front/images/logo_universite_poitiers.jpg)](https://www.univ-poitiers.fr)

[![](https://app.francoralite.net/static/francoralite_front/images/logo_mshs.jpg)](https://mshs.univ-poitiers.fr)


:::

Ce site a été réalisé avec [NakalaQuartoView](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view)