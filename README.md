# Dacunha-Castelle


# Fonds d'Archives Didier Dacunha-Castelle


Ce site a été généré avec Nakala-Quarto-View, un générateur de site web statique "maison" écrit en langage Python, utilisant l'API de Nakala pour requêter les données et Quarto pour construire le site.
Le rendu de ce site est visible ici: [https://mshs-poitiers.gitpages.huma-num.fr/crla/dacunha-castelle/](https://mshs-poitiers.gitpages.huma-num.fr/crla/dacunha-castelle/)

Le code source et les explications sont disponibles ici: [https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view](https://gitlab.huma-num.fr/mshs-poitiers/plateforme/nakala-quarto-view)

## Localisation des données
La collection des données est disponible sur [nakala.fr](https://nakala.fr/search/?q=dacunha-castelle)
Il existe un autre site de visualisation des mêmes données, réalisé avec NakalaPress: [https://archives-didier-dacunha-castelle.nakala.fr/](https://archives-didier-dacunha-castelle.nakala.fr/)

## fichiers spécifiques
le dossier env_quarto contient des fichiers spécifiques utilisés pour construire le site: contenu de pages d'accueil et "à propos", par exemple, et les images "fixes" utilisées dans ces pages.